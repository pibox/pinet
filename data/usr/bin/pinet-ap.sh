#!/bin/sh
# Pinet network configuration start/stop
#
# This is only called via pinet, not via init.d.
#
NETWORK_DIR=/etc/network
INTERFACES=$NETWORK_DIR/interfaces
INTERFACES_APONLY=$NETWORK_DIR/interfaces.aponly
INTERFACES_DEFAULT=$NETWORK_DIR/interfaces.wlan
HOSTAPD_APONLY=$NETWORK_DIR/hostapd.aponly
DHCP_APONLY=/etc/network/dhcpd.conf.aponly
LOGGER=/usr/bin/logger
PINET_DONE=/tmp/pinet.done

. /etc/init.d/functions

# Shutdown hostapd
doReboot()
{
    sleep 5
    nohup reboot -f &
}

# Shutdown hostapd
killAP()
{
    killall hostapd 2>/dev/null
    killall dhcpd 2>/dev/null
    rm -rf /var/lib/dhcp

    # Restore the interfaces file.
    cp $INTERFACES_DEFAULT $INTERFACES
}

# Start a default AP.  We assume that the
# network device is available.
startDefaultAP()
{
    rm -f "${PINET_DONE}"

    # The interfaces gets updated by the web interface
    # during network config.  So we don't need to restore
    # it in killAP().
    cp $INTERFACES_APONLY $INTERFACES

    mkdir -p /var/lib/dhcp
    touch /var/lib/dhcp/dhcpd.leases
    /sbin/ifup -a
    sleep 3
    hostapd -B $HOSTAPD_APONLY
    dhcpd -cf $DHCP_APONLY wlan0
}

# Are we on Pi W or something else?
boardType="$(getBoardType)"
if [[ "${boardType}" == "0" ]]
then
    IPIDX=1
else
    IPIDX=2
fi

# Now change the run state of the network.
case "$1" in
    start) 
        # Find out if hostapd is already running.
        doAP=`ps | grep hostapd | grep -v grep`

        if [ -z "${doAP}" ]; then
            ${LOGGER} "Restarting network in pair mode."
            /sbin/ifdown -a

            # Bring up loopback interface immediately.
            /sbin/ifup lo

            # We're in PAIR mode.
            # The user is trying to configure the AP and 
            # internet connection via a mobile device.

            # Start AP in default configuration.
            startDefaultAP
        fi
        ;;

    stop)
        ${LOGGER} "Stopping network pair mode and restarting normal mode."
        /sbin/ifdown wlan0
        killAP
        /sbin/ifup wlan0

        # Do this a few times to test that we get an IP assigned.
        CNT=0
        while [[ ${CNT} -lt 5 ]]; do
            ipaddr=$(ip route get ${IPIDX} 2>/dev/null | head -1 | sed 's/.*src //')
            if [[ "${ipaddr}" != "" ]]; then
                ${LOGGER} "Got IP address: ${ipaddr}"
                break
            fi
            CNT=$(( $CNT + 1 ))
            /sbin/ifdown wlan0
            /sbin/ifup wlan0
        done

        # Tell pinet we're done.
        touch "${PINET_DONE}"
        ;;

    *)
        ${LOGGER} "Usage: $0 {start|stop}"
        exit 1
esac
exit $?

