/*******************************************************************************
 * pinet
 *
 * pinet.h:  program main
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PINET_H
#define PINET_H

#include <gtk/gtk.h>

#define KEYSYMS_F       "/etc/pibox-keysyms"
#define KEYSYMS_FD      "data/pibox-keysyms"
#define F_IMAGE_DIR     "/etc/launcher/icons"
#define F_IMAGE_DIR_T   "data/icons"
#define F_PAIR_DIR      "/home/httpd/imwww/data"
#define F_PAIR_DIR_T    "data/imwww/data"
#define F_LOG_DIR       "log"
#define F_PAIR          "pair"
#define F_DEBUG         "debug"
#define F_NOAP          "noap"
#define F_DEFAULT_IMAGE "pinet-miot-logo-white-square-small.png"
#define F_SCRIPT_DIR    "/usr/bin"
#define F_SCRIPT_DIR_T  "data/usr/bin"
#define AP_START        "pinet-ap.sh start"
#define AP_STOP         "pinet-ap.sh stop"
#define WEBSERVER_START "runserver.sh -p -a &"
#define WEBSERVER_STOP  "runserver.sh -K"
#define DEFAULT_AP_IP   "192.168.36.1"
#define DEFAULT_PORT    1337
#define F_PW            "imwww.db"
#define F_PW_DIR        "/etc/"
#define F_PW_DIR_T      "data/"
#define LOGO_CENTER_X   91
#define LOGO_CENTER_Y   118
#ifndef SSID
#define DEFAULT_SSID    "piplayer"
#else
#define DEFAULT_SSID    SSID
#endif
#define DEFAULT_PSK     "[pw]"
#define F_PSK           "/etc/network/hostapd-psk.aponly"
#define F_PSK_T         "data/etc/network/hostapd-psk.aponly"
#define PINET_DONE      "/tmp/pinet.done"

// This is a hack.  See updatePSK() in pinet-gpio.c
#define F_GPIO_PW       "pibox0105"

// PID file: Also referenced by the associated init script.
#define PIDFILE         "/var/run/pinet.pid"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PINET_C
char    errBuf[256];
extern char     errBuf[];
#endif /* PINET_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pinet"
#define MAXBUF      4096

// Where the config file is located
#define F_CFG   "/etc/pinet.cfg"
#define F_CFG_T "data/pinet.cfg"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PINET_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "cli.h"
#include "utils.h"
#include "restartNetwork.h"
#ifdef HEADLESS
#include "gpio.h"
#include "handleAP.h"
#endif

#endif /* !PINET_H */

