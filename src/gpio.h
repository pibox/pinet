/*******************************************************************************
 * PiBox application library
 *
 * gpio.c:  Monitor GPIO events
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PINETGPIO_H
#define PINETGPIO_H

#ifdef HEADLESS
#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/
#define PINET_GPIO_AP                   1
#define PINET_GPIO_NORMAL               2

#define PINET_LED_OFF                   1
#define PINET_LED_ON                    2
#define PINET_LED_AP_ON                 3
#define PINET_LED_NETWORK_RESTARTING    4
#define PINET_LED_NORMAL                5

#define PINET_GPIO_PUSHBUTTON_PIN       23
#define PINET_GPIO_LED_PIN              24

#ifndef	CONSUMER_LED
#define	CONSUMER_LED	"LED"
#endif

#ifndef	CONSUMER_PUSHBUTTON
#define	CONSUMER_PUSHBUTTON	"PUSHBUTTON"
#endif

/*========================================================================
 * Variables
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PINETGPIO_C

extern gboolean startGPIOMonitor( void );
extern gboolean shutdownGPIOMonitor( void );
extern void enableLEDs( int state );
extern void disableLEDs( int state );
extern void setLEDState( int state );

#endif /* !PINETGPIO_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* HEADLESS */
#endif /* !PINETGPIO_H */
