/*******************************************************************************
 * PiBox application library
 *
 * restartNetwork.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define RESTARTNETWORK_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <pwd.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

static int restartIsRunning = 0;
static pthread_mutex_t restartNetworkMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t restartNetworkThread;
static sem_t restartSem;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Interal functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isProcessorRunning
 * Prototype:  int isProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of restartIsRunning variable.
 *========================================================================*/
static int
isProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &restartNetworkMutex );
    status = restartIsRunning;
    pthread_mutex_unlock( &restartNetworkMutex );
    return status;
}

/*========================================================================
 * Name:   _restartNetwork
 * Prototype:  void _restartNetwork( CLI_T * )
 *
 * Description:
 * Initiate a network restart.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *========================================================================*/
static void *
_restartNetwork( void *arg )
{
    char    filePath[PATH_MAX];
    char    *dirPath = F_SCRIPT_DIR;

    /* Spin, looking for messages to process. */
    restartIsRunning = 1;
    while( isCLIFlagSet(CLI_SERVER) )
    {
        sem_wait(&restartSem);

        /* 
         * A posted semaphore is used to break out of this loop.
         * Double check if we're being told to exit.
         */
        if( ! isCLIFlagSet(CLI_SERVER) )
        {
            unsetCLIFlag(CLI_RESTART);
            break;
        }

        /* Where are our startup scripts? */
        if ( !isCLIFlagSet( CLI_TEST) )
        {
            /* Stop the web server */
            sprintf(filePath, "%s/%s", dirPath, WEBSERVER_STOP);
            piboxLogger(LOG_INFO, "Stopping imwww: %s\n", filePath);
            system(filePath);

            /* Stop the AP, which restarts the network */
            sprintf(filePath, "%s/%s", dirPath, AP_STOP);
            piboxLogger(LOG_INFO, "Stopping AP: %s\n", filePath);
            system(filePath);
        }

        unsetCLIFlag(CLI_RESTART);
    }

    piboxLogger(LOG_INFO, "restartNetwork thread is exiting.\n");
    restartIsRunning = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Public functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   startNetworkProcessor
 * Prototype:  void startNetworkProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownProcessor().
 *========================================================================*/
void
startNetworkProcessor( void )
{
    int rc;

    /* Set up the semaphore used to bump the thread into action. */
    if (sem_init(&restartSem, 0, 0) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get restart processor semaphore: %s\n", strerror(errno));
        return;
    }

    setCLIFlag( CLI_SERVER );

    /* Create a thread to handle network restarts. */
    rc = pthread_create(&restartNetworkThread, NULL, &_restartNetwork, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create processor thread: %s\n", strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "Started restartNetwork process thread.\n");
    return;
}

/*========================================================================
 * Name:   shutdownNetworkProcessor
 * Prototype:  void shutdownNetworkProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownNetworkProcessor( void )
{
    int timeOut = 1;

    /* Wake sleeping processor, if necessary. */
    unsetCLIFlag( CLI_SERVER );
    if ( sem_post(&restartSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post restartSem semaphore.\n");

    while ( isProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "Timed out waiting on processor thread to shut down.\n");
            break;
        }
    }
    pthread_detach(restartNetworkThread);

    /* Clean up the semaphore used for this loop. */
    sem_destroy(&restartSem);

    piboxLogger(LOG_INFO, "restartNetwork thread has shut down.\n");
}

/*========================================================================
 * Name:   restartNetwork
 * Prototype:  void restartNetwork( void )
 *
 * Description:
 * Posts the semaphore that causes this thread to issue the network restart
 * request.
 *========================================================================*/
void
restartNetwork( void )
{
    if ( sem_post(&restartSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post restartSem semaphore.\n");
}
