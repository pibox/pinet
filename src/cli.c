/*******************************************************************************
 * pinet
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <pibox/utils.h>
#include <piboxnet/pnc.h>

#include "pinet.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;
#ifdef XEON
    char *str[3];
#endif

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* -D: Enable daemon mode. */
            case 'D':
                cliOptions.flags |= CLI_DAEMON;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = g_strdup(optarg);
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

#ifdef XEON
            /* -g: Set geometry, for testing purposes. */
            case 'g':
                if ( index(optarg, 'x') )
                {   
                    str[0] = strdup(optarg);
                    str[1] = strtok(str[0], "x");
                    str[2] = strtok(NULL, "x");
                    cliOptions.geometryW = atoi(str[1]);
                    cliOptions.geometryH = atoi(str[2]);
                    free(str[0]);
                }
                break;
#endif

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;

    /* Enable the threads. */
    cliOptions.flags |= CLI_SERVER;

    /* Try to get the MAC address for the wireless network interface. */
    cliOptions.macaddr = pncGetMacAddress("wlan0");

    /* Try to get IP address of the wired network, if any. */
    cliOptions.ethIP = getIPAddr("eth0");

#ifdef XEON
	/* Initialize geometry. */
	cliOptions.geometryW = -1;
	cliOptions.geometryH = -1;
#endif
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
    struct stat stat_buf;
    FILE *fd;
    char buf[MAXBUF];
    char *name;
    char *value;
    char cfg[PATH_MAX];

    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(cfg, "%s", F_CFG_T);
    else
        sprintf(cfg, "%s", F_CFG);

    // Check that the configuration file exists.
    if ( stat(cfg, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(cfg, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", cfg, strerror(errno));
            return;
        }

        // Each line is name:value pair
        while ( fgets(buf, MAXBUF, fd) != NULL )
        {
            piboxStripNewline(buf);

            name = strtok(buf, ":");
            if ( name == NULL )
                continue;
            fprintf(stderr, "Config file tag: %s\n", name);

            /*
             * None of these should have the ":" token in their value.
             */
            value = strtok(NULL, ":");
            if ( value==NULL )
                continue;

            // Set verbosity level
            if ( strncmp(name, "verbose", 7) == 0 )
                cliOptions.verbose = atoi(value);
        
            // Set debug file
            else if ( strncmp(name, "logfile", 7) == 0 )
            {
                if ( cliOptions.logFile != NULL )
                    g_free(cliOptions.logFile);
                cliOptions.logFile = g_strdup(value);
                cliOptions.flags |= CLI_LOGTOFILE;
            }
        }

        fclose(fd);
    }
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}
