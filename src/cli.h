/*******************************************************************************
 * PiBox service daemon
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_SERVER          0x00002    // Enable network restart thread.
#define CLI_RESTART         0x00004    // Restart in progress.
#define CLI_LOGTOFILE       0x00008    // Enable log to file
#define CLI_TEST            0x00020    // Enable test mode (read data files locally)
#define CLI_ROOT            0x00040    // Running as root
#define CLI_DEBOUNCE        0x00080    // Debounce check is enabled.
#define CLI_GPIO            0x00100    // Enabled gpio thread.
#define CLI_TOUCH           0x00200    // Using a touch screen
#define CLI_DAEMON          0x00400    // Enable daemon mode
#define CLI_SMALL_SCREEN    0x00800    // If set, we're on a small screen.
#define CLI_AP              0x01000    // Enable AP thread.
#define CLI_AP_STARTING     0x02000    // If set then the AP is starting.
#define CLI_AP_RUNNING      0x04000    // If set then the AP is running.

#define RUN_DIR             "/var/run/pinet"
#define LOCK_FILE           "/var/lock/pinet.lock"

typedef struct _cli_t {
    int     flags;              // Enable/disable features
    int     verbose;            // Sets the verbosity level for the application
    char    *logFile;           // Name of local file to write log to
    int     lockfd;             // Lock file file descriptor
    char    *macaddr;           // MAC address for the wireless network interface
    char    *ethIP;             // IP address for the wired network interface
#ifdef XEON
    int     geometryW;
    int     geometryH;
#endif
} CLI_T;

/*========================================================================
 * Text strings 
 *=======================================================================*/
/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#ifdef XEON
#define CLIARGS     "TDg:l:v:"
#else
#define CLIARGS     "TDl:v:"
#endif
#define USAGE \
"\n\
pinet [ -HT | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -T              Use test files (for debugging only) \n\
    -D              Enable daemon mode \n\
    -l filename     Enable local logging to named file \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
void initConfig( void );
void validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern void validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
