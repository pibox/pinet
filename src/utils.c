/*******************************************************************************
 * pinet
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <linux/if_link.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

/*========================================================================
 * Name:   random_at_most
 * Prototype:  long random_at_most( long max )
 *
 * Description:
 * Generate a truly random number of a range, as in [min..max].
 *
 * Returns:
 * The value in the closed interval [0, max].
 *
 * Notes:
 * Assumes 0 <= max <= RAND_MAX.
 * Borrowed from StackOverflow: https://stackoverflow.com/a/6852396
 *========================================================================*/
long 
random_at_most(long max) 
{
    unsigned long
        // max <= RAND_MAX < ULONG_MAX, so this is okay.
        num_bins = (unsigned long) max + 1,
        num_rand = (unsigned long) RAND_MAX + 1,
        bin_size = num_rand / num_bins,
        defect   = num_rand % num_bins;

    long x;
    do {
        x = random();
    }

    /* This is carefully written not to overflow */
    while (num_rand - defect <= (unsigned long)x);

    /* Truncated division is intentional */
    return x/bin_size;
}

/*========================================================================
 * Name:   genRandomPassword
 * Prototype:  char *genRandomPassword( void )
 *
 * Description:
 * Generate an 8 character random password.
 *
 * Returns:
 * Pointer to string.  
 *========================================================================*/
char *
genRandomPassword()
{
    char    *passwd;
    uuid_t  uuid;
    char    uuidStr[37];;
    char    *ptr, *last;
    long    index;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Generate a UUID */
    memset(uuidStr, 0, 37);
    uuid_generate(uuid);
    uuid_unparse(uuid, uuidStr);
    piboxLogger(LOG_INFO, "Generated uuid: %s\n", uuidStr);

    /* Strip "-" character" */
    ptr = uuidStr;
    last = uuidStr;
    while ( *ptr != '\0' ) 
    {
        if ( *ptr != '-' )
        {
            *last = *ptr;
            last++;
        }
        ptr++;
    }
    piboxLogger(LOG_INFO, "Stripped uuid: %s\n", uuidStr);

    /* 
     * Pick a random start location.  This is anywhere from the start to position 28
     * because
     * 1) a uuid is 36 characters.
     * 2) We stripped out 4 '-' characters. 
     * 3) We need 8 characters.
     */
    index = random_at_most(28);
    piboxLogger(LOG_INFO, "Index: %d\n", index);
    
    /* Create and fill the passwd buffer */
    passwd = (char *)calloc(1,9);

    /* Copy 8 characters from the uuid starting at the index into the passwd buffer */
    strncpy(passwd, (char *)(uuidStr+index), 8);

    /* return the generated password. */
    piboxLogger(LOG_INFO, "Generated pw: %s\n", passwd);
    return ( passwd );
}

/*========================================================================
 * Name:   mkPath
 * Prototype:  int mkPath( char *path )
 *
 * Description:
 * Recursively create the directories leading to the fully qualified path.
 *
 * Notes:
 * No return code.  Caller should stat the path to find out if it was created.
 *========================================================================*/
void
mkPath( char *path )
{

    char *sep = strrchr(path, '/');
    if(sep != NULL)
    {
        *sep = 0;
        mkPath(path);
        *sep = '/';
    }
    if ( (path == NULL) || (strlen(path)==0) )
        return;
    piboxLogger(LOG_INFO, "Creating '%s'\n", path);
    if ( mkdir(path, 0777) != 0 )
    {
        if( errno != EEXIST )
            piboxLogger(LOG_ERROR, "Error creating '%s': %s\n", path, strerror(errno));
        else
            piboxLogger(LOG_ERROR, "'%s' exists.\n", path);
    }
    else
        piboxLogger(LOG_INFO, "Created '%s'\n", path);
}

/*========================================================================
 * Name:   getIPAddr
 * Prototype:  char *getIPAddr( char *ifname )
 *
 * Description:
 * Return the IP address of the specified network interface.
 *
 * Returns:
 * The IP address as a character string.
 *========================================================================*/
char *
getIPAddr( char *ifname )
{
    struct ifaddrs *addrs, *tmp;
    struct sockaddr_in *pAddr;
    char ipaddr[17];

    memset(ipaddr, 0, 17);
    getifaddrs(&addrs);
    tmp = addrs;

    while (tmp)
    {
        if ( (strcmp(ifname, tmp->ifa_name)==0) && tmp->ifa_addr && (tmp->ifa_addr->sa_family == AF_INET) )
        {
            pAddr = (struct sockaddr_in *)tmp->ifa_addr;
            sprintf(ipaddr, "%s\n", inet_ntoa(pAddr->sin_addr));
            piboxLogger(LOG_INFO, "Found IP address for interface %s: %s\n", ifname, ipaddr);
            break;
        }
        tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);

    if ( strlen(ipaddr) > 0 )
        return strdup(ipaddr);
    else
    {
        piboxLogger(LOG_ERROR, "Can't find IP address for interface: %s\n", ifname);
        return NULL;
    }
}
