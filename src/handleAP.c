/*******************************************************************************
 * PiBox application library
 *
 * handleAP.c:  Manage start and stop of the AP.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define HANDLEAP_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <pwd.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

static int handleapIsRunning = 0;
static pthread_mutex_t handleAPMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t handleAPThread;
static sem_t handleapSem;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Interal functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isProcessorRunning
 * Prototype:  int isProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of handleapIsRunning variable.
 *========================================================================*/
static int
isProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &handleAPMutex );
    status = handleapIsRunning;
    pthread_mutex_unlock( &handleAPMutex );
    return status;
}

/*
 *========================================================================
 * Name:   updatePSK
 * Prototype:  void updatePSK( void )
 *
 * Description:
 * Update the hostapd psk file with the specified pw.
 *
 * Note:
 * This is a security hole:  how do we vary the PW if we can't tell the 
 * user what it is?  Possible solution:
 * 1. Create a list of possible PWs from 100 to 300 (for exmaple)
 * 2. Use GPIO LED that flashes three values, one for each digit of the pw.
 * 3. User uses lookup table (printed) to get pw.
 *========================================================================
 */
static void
updatePSK( void )
{
    FILE    *fd;
    char    *filename = F_PSK;
    char    *pw = F_GPIO_PW;

    piboxLogger(LOG_INFO, "Writing psk file: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Error opening psk file: %s\n", strerror(errno));
        return;
    }
    fprintf(fd, "00:00:00:00:00:00 %s\n", pw);
    fclose(fd);
}

/*========================================================================
 * Name:   _handleAP
 * Prototype:  void _handleAP( CLI_T * )
 *
 * Description:
 * Initiate a network handleap.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *========================================================================*/
static void *
_handleAP( void *arg )
{
    char    filePath[PATH_MAX];
    char    *dirPath = F_SCRIPT_DIR;  // Test mode is not supported because it can muck up the network.

    /* Spin, looking for messages to process. */
    handleapIsRunning = 1;
    while( isCLIFlagSet(CLI_AP) )
    {
        sem_wait(&handleapSem);
        piboxLogger(LOG_INFO, "handleAP semaphore arrived.\n");

        /* 
         * A posted semaphore is used to break out of this loop.
         * Double check if we're being told to exit.
         */
        if( ! isCLIFlagSet(CLI_AP) )
        {
            piboxLogger(LOG_INFO, "CLI_AP is not set - exiting _handleAP().\n");
            break;
        }

        /*
         * If we're already running the AP then ignore the request.
         */
        if( isCLIFlagSet(CLI_AP_RUNNING) )
        {
            piboxLogger(LOG_INFO, "CLI_AP_RUNNING is set - ignoring request.\n");
            continue;
        }

        piboxLogger(LOG_INFO, "Calling enableLEDs.\n");
        enableLEDs( PINET_LED_AP_ON );

        setCLIFlag(CLI_AP_STARTING);
        setCLIFlag(CLI_AP_RUNNING);

        /* Set the PW for the AP */
        updatePSK();

        /* Start the AP: The "imwww" package install created stamp files for us.  */
        sprintf(filePath, "%s/%s", dirPath, AP_START);
        piboxLogger(LOG_INFO, "Starting AP: %s\n", filePath);
        system(filePath);
    
        /* Start the web server */
        sprintf(filePath, "%s/%s", dirPath, WEBSERVER_START);
        piboxLogger(LOG_INFO, "Starting imwww: %s\n", filePath);
        system(filePath);

        unsetCLIFlag(CLI_AP_STARTING);
    }

    unsetCLIFlag(CLI_AP_RUNNING);

    piboxLogger(LOG_INFO, "handleAP thread is exiting.\n");
    handleapIsRunning = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Public functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   startNetworkProcessor
 * Prototype:  gboolean startNetworkProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Returns:
 * False on error.  True on success.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownProcessor().
 *========================================================================*/
gboolean
startAPProcessor( void )
{
    int rc;

    /* Set up the semaphore used to bump the thread into action. */
    if (sem_init(&handleapSem, 0, 0) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get AP processor semaphore: %s\n", strerror(errno));
        return(FALSE);
    }

    setCLIFlag( CLI_AP );

    /* Create a thread to handle network handleaps. */
    rc = pthread_create(&handleAPThread, NULL, &_handleAP, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create processor thread: %s\n", strerror(rc));
        return(FALSE);
    }
    piboxLogger(LOG_INFO, "Started AP process thread.\n");
    return(TRUE);
}

/*========================================================================
 * Name:   shutdownAPProcessor
 * Prototype:  void shutdownAPProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownAPProcessor( void )
{
    int timeOut = 1;

    /* Wake sleeping processor, if necessary. */
    unsetCLIFlag( CLI_AP );
    if ( sem_post(&handleapSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post handleapSem semaphore.\n");

    while ( isProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "Timed out waiting on processor thread to shut down.\n");
            break;
        }
    }
    pthread_detach(handleAPThread);
    unsetCLIFlag(CLI_AP_RUNNING);

    /* Clean up the semaphore used for this loop. */
    sem_destroy(&handleapSem);

    piboxLogger(LOG_INFO, "handleAP thread has shut down.\n");
}

/*========================================================================
 * Name:   handleAP
 * Prototype:  void handleAP( void )
 *
 * Description:
 * Posts the semaphore that causes this thread to issue the network handleap
 * request.
 *========================================================================*/
void
handleAP( void )
{
    piboxLogger(LOG_INFO, "Post handleAP semaphore.\n");
    if ( sem_post(&handleapSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post handleapSem semaphore.\n");
}
