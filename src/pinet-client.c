/*******************************************************************************
 * pinet
 *
 * pinet-client.c:  program main for WiFi client configuration.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PINET_C

#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>
#include <piboxnet/pnc.h>

#include "pinet-client.h"

/* Local prototypes */
gboolean imageTouchGTK( gpointer arg );
void pinetExit( void );

static pthread_mutex_t     listMutex = PTHREAD_MUTEX_INITIALIZER;
static guint               homeKey = -1;
static GtkWidget           *window;
static GtkWidget           *ssid_entry;
static GtkWidget           *ssid_list;
static GtkWidget           *ssid_security;
static GtkWidget           *keyboard;
static GtkWidget           *pw_entry;
static GtkWidget           *active_entry;
static GtkWidget           *hide_button;
static GtkWidget           *mac_button;
static GSList              *bssList = NULL;
static GSList              *macList = NULL;
static guint               listSize = 6;
static guint               showMac = 0;

/*
 *========================================================================
 * Name:   slist_back
 * Prototype:  void slist_back( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Move back through the list of SSIDs.
 *========================================================================
 */
static gboolean
slist_back(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint offset;

    offset = gtk_simplelist_get_offset(ssid_list);
    if (offset > 0 )
        gtk_simplelist_set_offset(ssid_list, offset - 1);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_forward
 * Prototype:  void slist_forward( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Move forward through the list of SSIDs.
 *========================================================================
 */
static gboolean
slist_forward(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint offset;
    guint maxLength;

    offset = gtk_simplelist_get_offset(ssid_list);
    maxLength = g_slist_length(bssList);
    piboxLogger(LOG_INFO, "offset: %d, maxLength: %d\n", offset, maxLength);
    if ( (offset+listSize) < maxLength )
        gtk_simplelist_set_offset(ssid_list, offset + 1);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_mac
 * Prototype:  void slist_mac( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Either show the MAC or the list of SSIDs.
 *========================================================================
 */
static gboolean
slist_mac(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    pthread_mutex_lock( &listMutex );

    /* Based on current state, choose what to switch to. */
    switch(showMac)
    {
        case 0:
            /* Switch to the device MAC address. */
            gtk_simplelist_set_rows(ssid_list, 1);
            gtk_simplelist_set(ssid_list, macList);
            gtk_button_set_label(GTK_BUTTON(mac_button), S_SSIDLIST);
            break;

        case 1:
            /* Switch to the list of SSIDs. */
            gtk_simplelist_set_rows(ssid_list, listSize);
            gtk_simplelist_set(ssid_list, bssList);
            gtk_button_set_label(GTK_BUTTON(mac_button), S_SHOWMAC);
            break;
    }

    /* Toggle the state. */
    showMac = showMac ^ 1;

    pthread_mutex_unlock( &listMutex );
    return(TRUE);
}

/*========================================================================
 * Name:   copyBSSList
 * Prototype:  gpointer copyBSSList( gconstpointer src, gpointer data )
 *
 * Description:
 * Copy the SSID name from the BSS list.
 *========================================================================*/
static gpointer
copyBSSList( gconstpointer src, gpointer data )
{
    BSS_T *entry = (BSS_T *)src;
    char *new_entry;

    new_entry = strdup(entry->ssid);
    piboxLogger(LOG_INFO, "SSID: %s\n", new_entry);
    return new_entry;
}

/*
 *========================================================================
 * Name:   updateScan
 * Prototype:  gboolean updateScan( gpointer )
 *
 * Description:
 * Called periodically from a glib timer to cause the scan area to be
 * updated.
 *========================================================================
 */
gboolean updateScan (gpointer data)
{
    GSList *new_bss = NULL;

    piboxLogger(LOG_TRACE2, "Getting BSS copy.\n");
    new_bss = getBSSList();
    if ( !showMac && (new_bss != NULL) )
    {
        pthread_mutex_lock( &listMutex );
        // This should add any saved hidden SSIDs, if found in the scan.
        piboxLogger(LOG_TRACE2, "Found BSS data available.\n");
        g_slist_free_full(bssList, freeBSSList);
        // Convert to GList->ssid from bssList:(BSS_T)entry->ssid
        bssList = g_slist_copy_deep( pncGetBSSList(), copyBSSList, NULL );
        gtk_simplelist_set(ssid_list, bssList);
        pthread_mutex_unlock( &listMutex );
    }
    else
    {
        piboxLogger(LOG_TRACE2, "No BSS data available.\n");
    }

    return TRUE;
}

/*
 *========================================================================
 * Name:   set_active
 * Prototype:  void set_active( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button release (as with a mouse).
 *========================================================================
 */
static gboolean
set_active(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    piboxLogger(LOG_INFO, "Entered: %d.\n", GPOINTER_TO_INT(user_data));
    active_entry = widget;
    return(TRUE);
}

/*
 *========================================================================
 * Name:   hide_press
 * Prototype:  void hide_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a press of the hide button.
 *========================================================================
 */
static gboolean
hide_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint toggle;

    gtk_keyboard_increment_page(hide_button);
    toggle = gtk_textline_get_hidden(pw_entry);
    piboxLogger(LOG_INFO, "toggle: %d\n", toggle);
    toggle = toggle ^ 1;
    piboxLogger(LOG_INFO, "toggled: %d\n", toggle);
    gtk_textline_set_hidden(pw_entry, toggle);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_press
 * Prototype:  void slist_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handle user selection of a simplelist entry.  This puts the selection
 * in the Selected field and highlights the selection.
 *========================================================================
 */
static gboolean
slist_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *ssid;

    x = event->x;
    y = event->y;
    piboxLogger(LOG_INFO, "Position: %f x %f\n", x, y);
    gtk_simplelist_press(ssid_list, x, y, 1);
    ssid = gtk_simplelist_get_row(ssid_list, x, y);
    gtk_textline_set(ssid_entry, ssid);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_release
 * Prototype:  void slist_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Update simplelist entry when user releases the selection press.
 * This just removes the temporary highlight of the selection.
 *========================================================================
 */
static gboolean
slist_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;

    x = event->x;
    y = event->y;
    gtk_simplelist_press(ssid_list, x, y, 0);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button press (as with a mouse).
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *filename;
    char        *key;
    char        *buf = NULL;
    char        *entry;
    char        *ssidStr;
    char        *pwStr;

    entry = gtk_textline_get(active_entry);
    x = event->x;
    y = event->y;
    piboxLogger(LOG_INFO, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(widget);
    key = gtk_keyboard_get_key(keyboard);
    if ( key )
    {
        piboxLogger(LOG_INFO, "Key: %s\n", key);
        if ( strcasecmp(key, "enter") == 0 )
        {
            /* 
             * ENTER is the Accept button for this app.
             * Clear the current entry - we'll get it again in a moment. 
             */
            free(entry);

            /* Don't free entry on function exit. */
            entry = NULL;

            /* Retrieve the configured SSID and password. */
            ssidStr = gtk_textline_get(ssid_entry);
            pwStr = gtk_textline_get(pw_entry);

            /* If both are configured, save the configuration. */
            if ( (ssidStr!=NULL) && (strlen(ssidStr)>0) &&
                 (pwStr!=NULL)   && (strlen(pwStr)>0) )
            {
                pncSetWPAField( PNC_ID_WPA_SSID, ssidStr );
                pncSetWPAField( PNC_ID_WPA_PSK, pwStr );

                /* Choose a filename to save the configuration to. */
                if ( isCLIFlagSet( CLI_TEST) )
                    filename = strdup( F_WPA_TEST );
                else
                    filename = pncGetFilename( PNC_ID_WPASUPPLICANT );
                if ( filename )
                {
                    pncSaveWPA(filename);
                    free(filename);
                }
                else
                {
                    piboxLogger(LOG_INFO, "Missing filename to save to.\n");
                }
            }

            /* Cleanup as needed. */
            if ( ssidStr!=NULL )
                free(ssidStr);
            if ( pwStr!=NULL )
                free(pwStr);
        }
        else if ( strcasecmp(key, "clear") == 0 )
        {
            buf = (char *)calloc(1, 2);
        }
        else if ( strcasecmp(key, "backspace") == 0 )
        {
            if (strlen(entry) > 0)
            {
                memset((char *)(entry+strlen(entry)-1), 0, 1);
                buf = (char *)calloc(1, strlen(entry) + 2);
                sprintf(buf, "%s", entry );
            }
        }
        else if ( strcasecmp(key, "space") == 0 )
        {
            if (entry)
            {
                buf = (char *)calloc(1, strlen(entry) + 2);
                sprintf(buf, "%s ", entry );
            }
            else
            {
                buf = (char *)calloc(1, 2);
                sprintf(buf, " ");
            }
        }
        else if ( strcasecmp(key, "shift") == 0 )
        {
            gtk_keyboard_shift(keyboard);
        }
        else if ( strcasecmp(key, "page") == 0 )
        {
            gtk_keyboard_increment_page(keyboard);
        }
        else if ( strcasecmp(key, "alpha") == 0 )
        {
            gtk_keyboard_increment_page(keyboard);
        }

        else if ( entry )
        {
            buf = (char *)calloc(1, strlen(entry) + strlen(key) + 1);
            sprintf(buf, "%s%s", entry, key);
        }
        else
        {
            buf = (char *)calloc(1, strlen(key) + 1);
            sprintf(buf, "%s", key);
        }
        free(key);
        if ( buf ) 
        {
            gtk_textline_set(active_entry, buf);
            free(buf);
        }
    }
    else
        piboxLogger(LOG_INFO, "Key: Unknown\n");

    return(TRUE);
}

/*
 *========================================================================
 * Name:   button_release
 * Prototype:  void button_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button release (as with a mouse).
 *========================================================================
 */
static gboolean
button_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    x = event->x;
    y = event->y;
    piboxLogger(LOG_INFO, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    return(TRUE);
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * ========================================================================
 */
static void
sigHandler( int sig )
{
    switch (sig)
    {
        case SIGHUP:
            pinetExit();
            break;

        case SIGTERM:
            pinetExit();
            break;

        case SIGINT:
            pinetExit();
            break;

        case SIGQUIT:
            pinetExit();
            break;
    }
}

/*
 * ========================================================================
 * Name:   quit
 * Prototype:  gboolean quit( GtkWidget *window )
 *
 * Description:
 * Graceful exit.
 * ========================================================================
 */
static gboolean
quit(GtkWidget * window)
{
    gtk_main_quit();
    return(TRUE);
}

/*
 *========================================================================
 * Name:   pinetExit
 * Prototype:  void pinetExit( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pinetExit()
{
    /* Resart system network. */
    setCLIFlag(CLI_RESTART);
    restartNetwork();

    /* Wait a bit for network restart, but allow the animation to continue to spin till we quit. */
    (void)g_timeout_add(1000, (GSourceFunc)quit, NULL);
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( REGION_T *pt )
 *
 * Description:
 * Handler for absolute touch reports.  For PiNet, any touch just means
 * to exit.
 *========================================================================
 */
void
imageTouch( REGION_T *pt )
{
    g_idle_add( (GSourceFunc)imageTouchGTK, (gpointer)pt );
}

gboolean
imageTouchGTK( gpointer arg )
{
    pinetExit();
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be
 * handled.
 *
 * Notes:
 * Format is
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayTouch() == PIBOX_TOUCH )
    {
        setCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Touch screen support enabled.\n");
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Not a touch screen.\n");
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                // gtk_main_quit();
                pinetExit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            // gtk_main_quit();
            pinetExit();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                // gtk_main_quit();
                pinetExit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget   *vbox;
    GtkWidget   *hbox;
    GtkWidget   *button;
    GtkWidget   *label;
    GtkWidget   *entry;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // One column for all widgets.
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    // An hbox for SSID and Security
    hbox = gtk_hbox_new (FALSE, 10);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

    label = gtk_label_new("Selected");
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);

    // The Selected SSID as a textline widget.
    ssid_entry = gtk_textline_new();
    gtk_box_pack_start (GTK_BOX (hbox), ssid_entry, TRUE, TRUE, 0);
    gtk_textline_set_fontsize(ssid_entry, 12);
    gtk_textline_set_maxlength(ssid_entry, 32);
    gtk_widget_add_events(ssid_entry, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(ssid_entry), "button_press_event", G_CALLBACK(set_active), GINT_TO_POINTER(1));

    // An hbox for password.
    hbox = gtk_hbox_new (FALSE, 6);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

    label = gtk_label_new("Password");
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);

    pw_entry = gtk_textline_new();
    gtk_box_pack_start (GTK_BOX (hbox), pw_entry, TRUE, TRUE, 0);
    gtk_textline_set_fontsize(pw_entry, 12);
    gtk_textline_set_maxlength(pw_entry, 32);
    gtk_widget_add_events(pw_entry, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(pw_entry), "button_press_event", G_CALLBACK(set_active), GINT_TO_POINTER(2));

    hide_button = gtk_keyboard_new("hide-button.json");
    gtk_widget_set_size_request( hide_button, 30, -1);
    gtk_widget_add_events(hide_button, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_box_pack_start (GTK_BOX (hbox), hide_button, FALSE, TRUE, 0);
    g_signal_connect(G_OBJECT(hide_button), "button_press_event", G_CALLBACK(hide_press), GINT_TO_POINTER(0));

    // A drop down menu for Security selection
    ssid_security = gtk_combo_box_entry_new_text();
    gtk_combo_box_append_text(GTK_COMBO_BOX(ssid_security), "WPA2 Personal");
    gtk_combo_box_append_text(GTK_COMBO_BOX(ssid_security), "WPA2 Enterprise");
    gtk_box_pack_start (GTK_BOX (vbox), ssid_security, FALSE, TRUE, 0);
    entry = gtk_bin_get_child (GTK_BIN(ssid_security));
    gtk_entry_set_editable (GTK_ENTRY(entry), FALSE);

    label = gtk_label_new("Available SSIDs");
    gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);

    // The Available SSIDs as a simplelist widget.
    ssid_list = gtk_simplelist_new();
    gtk_box_pack_start (GTK_BOX (vbox), ssid_list, TRUE, TRUE, 0);
    gtk_widget_add_events(ssid_list, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_simplelist_set_fontsize(ssid_list, 18);
    gtk_simplelist_set_rows(ssid_list, listSize);
    g_signal_connect(G_OBJECT(ssid_list), "button_press_event", G_CALLBACK(slist_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(ssid_list), "button_release_event", G_CALLBACK(slist_release), GINT_TO_POINTER(0));

    // An hbox for buttons.
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

    // The Forward/Back/Mac buttons to move through the list of SSIDs.
    button = gtk_button_new_with_label ("Back");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_back), 0);

    mac_button = gtk_button_new_with_label (S_SHOWMAC);
    gtk_box_pack_start (GTK_BOX (hbox), mac_button, TRUE, TRUE, 0);
    gtk_widget_show(mac_button);
    g_signal_connect(G_OBJECT(mac_button), "button_press_event", G_CALLBACK(slist_mac), 0);

    button = gtk_button_new_with_label ("Forward");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_forward), 0);

    // The keyboard widget for entering a Hidden SSID.
    keyboard = gtk_keyboard_new("keyboard-us.json");
    gtk_keyboard_set_fontsize(keyboard, 48);
    gtk_widget_add_events(keyboard, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_box_pack_start (GTK_BOX (vbox), keyboard, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(keyboard), "button_press_event", G_CALLBACK(button_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(keyboard), "button_release_event", G_CALLBACK(button_release), GINT_TO_POINTER(0));

    // Button to Exit the app.
    hbox = gtk_hbox_new (TRUE, 0);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

    button = gtk_button_new_with_label ("Quit");
    gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK (pinetExit), NULL);

    // Make the SSID the active entry for keyboard output.
    active_entry = ssid_entry;

    /* Make the main window die when destroy is issued. */
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK (pinetExit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    if ( cliOptions.geometryW != -1 )
        gtk_window_set_default_size(GTK_WINDOW(window), cliOptions.geometryW, cliOptions.geometryH);
    else
        gtk_window_set_default_size(GTK_WINDOW(window), 2048, 1024);
    gtk_window_set_title(GTK_WINDOW(window), "pinet");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    cwd[512];
    guint   timer;

    GtkWidget *window;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Handle signals */
    signal(SIGQUIT, sigHandler);
    signal(SIGTERM, sigHandler);
    signal(SIGHUP, sigHandler);
    signal(SIGINT, sigHandler);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    
    /* Get the current working directory. */
    memset(cwd, 0, 512);
    getcwd(cwd, 512);

    /* Read environment config for keyboard behaviour */
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();

    /*
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }

    /* Load existing wpa_supplicant configuration. */
    pncInit( isCLIFlagSet(CLI_TEST), NULL);
    pncLoadWPA();
    pncGenWifiList();

    /*
     * We need to initialize all these functions so that gtk knows
     * to be thread-aware.
     */
    gdk_threads_init();
    gdk_threads_enter();

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
    {
        piboxLogger(LOG_INFO, "Using gtkrc %s.\n", F_GTKRC);
        gtk_rc_parse(F_GTKRC);
    }
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    if ( cliOptions.geometryW == -1 )
        gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);
    gtk_keyboard_press(keyboard, -1, -1, 0);
    gtk_keyboard_press(hide_button, -1, -1, 0);

    /* Start threads */
    startScanProcessor();

    /* Start a timer that updates the SSID list. */
    timer = g_timeout_add(10000, (GSourceFunc)updateScan, 0);

    gtk_main();
    gdk_threads_leave();
    g_source_remove(timer);

    /* Stop threads */
    shutdownScanProcessor();

    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}

