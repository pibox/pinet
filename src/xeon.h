/*******************************************************************************
 * pinet
 *
 * xeon.h:  custom widget for configuring networking on a Xeon phone.
 *
 * License: BSD0
 *
 * Based on https://sites.cc.gatech.edu/data_files/public/doc/gtk/tutorial/gtk_tut-20.html
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __XEONWIDGET_H
#define __XEONWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_XEON(obj) GTK_CHECK_CAST(obj, gtk_xeon_get_type (), GtkXeon)
#define GTK_XEON_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_xeon_get_type(), GtkXeonClass)
#define GTK_IS_XEON(obj) GTK_CHECK_TYPE(obj, gtk_xeon_get_type())

typedef struct _GtkXeon GtkXeon;
typedef struct _GtkXeonClass GtkXeonClass;

struct _GtkXeon {
  GtkVBox parent;
  GtkDrawingArea **darea;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  GSList *wifi_devices;
  GSList *bss_list;
  GHashTable *channels;
};

struct _GtkXeonClass {
  GtkVBoxClass parent_class;
  void (*xeon) (GtkXeon *xeoninst);
};

/* Prototypes */
GtkType    gtk_xeon_get_type(void);
GtkWidget *gtk_xeon_new( void );
void       gtk_xeon_update_bss( GtkXeon *xeon );
char      *gtk_xeon_get_selection( GtkXeon *xeon );

G_END_DECLS

#endif /* __XEONWIDGET_H */
