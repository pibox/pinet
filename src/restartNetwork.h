/*******************************************************************************
 * PiBox application library
 *
 * restartNetwork.c:  Manage the request to piboxd to restart the network.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef RESTARTNETWORK_H
#define RESTARTNETWORK_H

#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef RESTARTNETWORK_C

#endif /* !RESTARTNETWORK_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef RESTARTNETWORK_C

extern void startNetworkProcessor( void );
extern void shutdownNetworkProcessor( void );
extern void restartNetwork( void );

#endif /* !RESTARTNETWORK_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !RESTARTNETWORK_H */
