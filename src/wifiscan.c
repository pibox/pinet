/*******************************************************************************
 * pinet
 *
 * wifiscan.c:  Manage background wifi scans
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define WIFISCAN_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <glib.h>
#include <pibox/log.h>
#include <piboxnet/pnc.h>

/* Module prototypes */
void genBSSList( void );

static int mgrIsRunning = 0;
static int mgrThreadIsRunning = 0;
pthread_mutex_t mgrProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t mgrProcessorThread;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   copyBSSList
 * Prototype:  gpointer copyBSSList( gconstpointer src, gpointer data )
 *
 * Description:
 * Copy an element of the BSS list.
 *========================================================================*/
static gpointer
copyBSSList( gconstpointer src, gpointer data )
{
    BSS_T *entry = (BSS_T *)src;
    BSS_T *new_entry;

    new_entry = (BSS_T *)calloc(1,sizeof(BSS_T));
    new_entry->channel = entry->channel;
    new_entry->signal  = entry->signal;
    new_entry->ssid    =  strdup(entry->ssid);
    return new_entry;
}

/*========================================================================
 * Name:   isMgrProcessorRunning
 * Prototype:  int isMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
static int
isMgrProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   isMgrTheadProcessorRunning
 * Prototype:  int isMgrTheadProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
int
isMgrThreadRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrThreadIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setMgrProcessorRunning
 * Prototype:  int setMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of mgrIsRunning variable.
 *========================================================================*/
static void
setMgrProcessorRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   setMgrThreadRunning
 * Prototype:  int setMgrThreadRunning( void )
 *
 * Description:
 * Thread-safe set of mgrTheadIsRunning variable.
 *========================================================================*/
static void
setMgrThreadRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrThreadIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   mgrProcessor
 * Prototype:  void mgrProcessor( void * )
 *
 * Description:
 * Peform and retrieve wifi scan data.
 *
 * Input Arguments:
 * void *arg    Not used, but required for threads.
 *========================================================================*/
static void *
mgrProcessor( void *arg )
{
    int cnt = 0;

    setMgrProcessorRunning(1);
    setMgrThreadRunning(1);
    while ( isMgrProcessorRunning() )
    {
        /* 
        * Scan wifi channels
        */
        if ( cnt == 20 )
        {
            piboxLogger(LOG_INFO, "Generating BSS list.\n");
            genBSSList();
        }
        usleep(100000);
        cnt++;
    }

    piboxLogger(LOG_INFO, "Mgr thread is exiting.\n");
    setMgrThreadRunning(0);
    return(0);
}

/*========================================================================
 * Name:   startMgr
 * Prototype:  void startMgr( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownMgrProcessor().
 *========================================================================*/
void
startScanProcessor( void )
{
    int rc;

    // Prevent running two at once.
    if ( isMgrProcessorRunning() )
        return;

    // Must get a list of wifi devices, which is stored in the PNC library.
    piboxLogger(LOG_INFO, "Getting wifi list for PNC library.\n");

    /* Create a thread to expire streams. */
    rc = pthread_create(&mgrProcessorThread, NULL, &mgrProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create mgr thread: %s\n", strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "Started mgr thread.\n");
    return;
}

/*========================================================================
 * Name:   shutdownMgrProcessor
 * Prototype:  void shutdownMgrProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownScanProcessor( void )
{
    int timeOut = 0;

    setMgrProcessorRunning(0);
    if ( isMgrThreadRunning() )
    {
        while ( isMgrThreadRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 5)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on mgr thread to shut down.\n");
                pthread_cancel(mgrProcessorThread);
                piboxLogger(LOG_INFO, "mgrProcessor forced shut down.\n");
                return;
            }
        }
        pthread_detach(mgrProcessorThread);
    }
    piboxLogger(LOG_INFO, "mgrProcessor shut down.\n");
}

/*========================================================================
 * Name:   genBSSList
 * Prototype:  void genBSSList( void )
 *
 * Description:
 * Thread-safe generation of the BSS list.
 *========================================================================*/
void
genBSSList( void )
{
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return;
    pncGenBSSList();
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   getBSSList
 * Prototype:  void getBSSList( void )
 *
 * Description:
 * Retrieve a copy of the current set of BSS entries.
 * Caller is responsible for clearing the list with g_slist_free_full().
 * This is the thread safe retrieval of the list.
 *
 * Returns:
 * A copy of the BSS entries list or NULL if the lock cannot be acquired.
 *========================================================================*/
GSList *
getBSSList( void )
{
    GSList *bss;
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return NULL;
    bss = g_slist_copy_deep( pncGetBSSList(), copyBSSList, NULL );
    pthread_mutex_unlock( &mgrProcessorMutex );
    return bss;
}

/*========================================================================
 * Name:   freeBSSList
 * Prototype:  void freeBSSList( void )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
void
freeBSSList( gpointer data )
{
    g_free(data);
}
