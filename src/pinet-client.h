/*******************************************************************************
 * pinet
 *
 * pinet-client.h:  program main for WiFi client configuration.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PINET_H
#define PINET_H

#include <gtk/gtk.h>

#define KEYSYMS_F       "/etc/pibox-keysyms"
#define KEYSYMS_FD      "data/pibox-keysyms"
#define F_GTKRC         "data/gtkrc"
#define F_WPA_TEST      "/tmp/wpa_supplicant.conf"
#define S_SHOWMAC       "Show MAC"
#define S_SSIDLIST      "SSID List"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PINET_C
char    errBuf[256];
extern char     errBuf[];
#endif /* PINET_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pinet"
#define MAXBUF      4096

// Where the config file is located
#define F_CFG   "/etc/pinet.cfg"
#define F_CFG_T "data/pinet.cfg"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PINET_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pigtk/pigtk-keyboard.h>
#include <pigtk/pigtk-simplelist.h>
#include <pigtk/pigtk-textline.h>
#include "cli.h"
#include "utils.h"
#include "restartNetwork.h"
#include "wifiscan.h"

#endif /* !PINET_H */

