/*******************************************************************************
 * PiBox application library
 *
 * gpio.c:  Monitor GPIO events
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PINETGPIO_C
#ifdef HEADLESS

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gpiod.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <pwd.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

void setLEDState( int state );

static int gpioIsRunning = 0;
static pthread_mutex_t gpioMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t gpioStateMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t gpioThread;
static int gpio_state = PINET_GPIO_NORMAL;
static struct gpiod_chip *chip = NULL;
static struct gpiod_line *led_line;
static guint led_timer = 0;
static int led_state = PINET_LED_OFF;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Interal functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   deboundReset
 * Prototype:  int deboundReset( void )
 *
 * Description:
 * Thread-safe reset of debounce timeout.
 *========================================================================*/
static gboolean
debounceReset(void)
{
    unsetCLIFlag(CLI_DEBOUNCE);
    return(FALSE);
}

/*========================================================================
 * Name:   isProcessorRunning
 * Prototype:  int isProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of gpioIsRunning variable.
 *========================================================================*/
static int
isProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &gpioMutex );
    status = gpioIsRunning;
    pthread_mutex_unlock( &gpioMutex );
    return status;
}

/*========================================================================
 * Name:   _flashLEDs
 * Prototype:  gboolean _flashLEDs( void )
 *
 * Description:
 * Flash the LEDs based on the current LED state.  This is called as a timer
 * function.  It either repeats as needed or runs through once and completes.
 *
 * Notes:
 * Passing the state instead of using the global avoids thread sync 
 * problems.
 *
 * Notes:
 * Blink patterns are as follows:
 * 1 second (500ms on/500ms off) blink: AP mode is active
 * 2 200ms flashes/second: network is restarting
 * 2s solid followed by 10 200ms flashes followed by no light: returning to client mode
 *========================================================================*/
static gboolean 
_flashLEDs( void )
{
	int i;
	int ret;

    // Internal use of led_state
    static int internal_state = 0;

    /* Tracks NORMAL LED state changes through multiple timer runs */
    static int normal_state = 0;

    piboxLogger(LOG_INFO, "Entered.\n");

    pthread_mutex_lock( &gpioStateMutex );
    internal_state = led_state;
    pthread_mutex_unlock( &gpioStateMutex );

    piboxLogger(LOG_INFO, "Internal state=%d\n", internal_state);

    switch(internal_state)
    {
        // Turn off LEDs
        case PINET_LED_OFF:
            piboxLogger(LOG_INFO, "PINET_LED_OFF.\n");
            ret = gpiod_line_set_value(led_line, 0);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to disable LED.\n");
            normal_state = 0;
            led_timer = 0;
            return(FALSE);
            break;

        // Turn on LEDs
        case PINET_LED_ON:
            piboxLogger(LOG_INFO, "PINET_LED_ON.\n");
            ret = gpiod_line_set_value(led_line, 1);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to disable LED.\n");
            normal_state = 0;
            return(TRUE);
            break;

        // 750ms on, then off.
        case PINET_LED_AP_ON:
            piboxLogger(LOG_INFO, "PINET_LED_AP_ON.\n");
            normal_state = 0;
            ret = gpiod_line_set_value(led_line, 1);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
            usleep( 750000 );
            ret = gpiod_line_set_value(led_line, 0);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to disable LED.\n");
            return(TRUE);
            break;

        // 180ms on, 60ms off, 180ms on, off
        case PINET_LED_NETWORK_RESTARTING:
            piboxLogger(LOG_INFO, "PINET_LED_NETWORK_RESTARTING.\n");
            normal_state = 0;
            ret = gpiod_line_set_value(led_line, 1);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
            usleep( 180000 );
            ret = gpiod_line_set_value(led_line, 0);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to disable LED.\n");
            usleep( 60000 );
            ret = gpiod_line_set_value(led_line, 1);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
            usleep( 180000 );
            ret = gpiod_line_set_value(led_line, 0);
            if (ret < 0)
                piboxLogger(LOG_ERROR, "Failed to disable LED.\n");
            return(TRUE);
            break;

        case PINET_LED_NORMAL:
            piboxLogger(LOG_INFO, "PINET_LED_NORMAL.\n");
            switch(normal_state)
            {
                case 0:
                    piboxLogger(LOG_INFO, "normal_state = %d.\n", normal_state);
                    // On for 2 seconds.
                    ret = gpiod_line_set_value(led_line, 1);
                    if (ret < 0)
                        piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
                    normal_state++;
                    return(TRUE);
                    break;

                case 1:
                    piboxLogger(LOG_INFO, "normal_state = %d.\n", normal_state);
                    normal_state++;
                    return(TRUE);
                    break;

                case 2:
                    piboxLogger(LOG_INFO, "normal_state = %d.\n", normal_state);
                    // Blink 4 times really fast.
                    ret = gpiod_line_set_value(led_line, 0);
                    if (ret < 0)
                        piboxLogger(LOG_ERROR, "Failed to enable LED.\n");

                    for( i=0; i<4; i++)
                    {
                        // 200ms on, off 50ms
                        ret = gpiod_line_set_value(led_line, 1);
                        if (ret < 0)
                            piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
                        usleep( 200000 );
                        ret = gpiod_line_set_value(led_line, 0);
                        if (ret < 0)
                            piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
                        if ( i < 4 )
                            usleep( 50000 );
                    }
                    normal_state++;
                    return(TRUE);
                    break;

                case 3:
                    piboxLogger(LOG_INFO, "normal_state = %d.\n", normal_state);
                    // Blink 4 times really fast.
                    ret = gpiod_line_set_value(led_line, 0);
                    if (ret < 0)
                        piboxLogger(LOG_ERROR, "Failed to enable LED.\n");

                    for( i=0; i<4; i++)
                    {
                        // 200ms on, off 50ms
                        ret = gpiod_line_set_value(led_line, 1);
                        if (ret < 0)
                            piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
                        usleep( 200000 );
                        ret = gpiod_line_set_value(led_line, 0);
                        if (ret < 0)
                            piboxLogger(LOG_ERROR, "Failed to enable LED.\n");
                        if ( i < 4 )
                            usleep( 50000 );
                    }

                    /* Next iteration will shut off LED and kill timer. */
                    normal_state = 0;
                    setLEDState( PINET_LED_OFF );
                    return(TRUE);
                    break;

        		default:
					piboxLogger(LOG_ERROR, "Unknown normal state: %d\n", normal_state);
                    normal_state = 0;
                    setLEDState( PINET_LED_OFF );
            		return(TRUE);
            		break;
            }
            break;

        default:
			piboxLogger(LOG_ERROR, "Unknown internal state: %d\n", internal_state);
            normal_state = 0;
            setLEDState( PINET_LED_OFF );
            return(TRUE);
            break;
    }
}

/*========================================================================
 * Name:   gpio
 * Prototype:  void gpio( CLI_T * )
 *
 * Description:
 * Monitor GPIO events for the pushbutton. If the button is pressed, then
 * toggle the network state (client or AP mode) and set the LED blink 
 * pattern accordingly.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *========================================================================*/
static void *
gpio( void *arg )
{
    unsigned int line_num = PINET_GPIO_PUSHBUTTON_PIN; // GPIO Pin #25
    struct timespec ts = { 0, 200000000 }; // 200ms timeout waiting on events
    struct gpiod_line_event event;
    struct gpiod_line *line;
	int ret;

    line = gpiod_chip_get_line(chip, line_num);
    if (!line)
    {
        piboxLogger(LOG_ERROR, "Get line failed for GPIO pin %d\n", line_num);
        return(0);
    }

    ret = gpiod_line_request_rising_edge_events(line, CONSUMER_PUSHBUTTON);
    if (ret < 0)
    {
        piboxLogger(LOG_ERROR, "Request event notification failed for GPIO pin %d\n", line_num);
        gpiod_line_release(line);
        return(0);
    }

    /* Spin, waiting on GPIO events. */
    gpioIsRunning = 1;
    while( isCLIFlagSet(CLI_GPIO) )
    {
        /* Wait on an event on the specified GPIO pin. */
        ret = gpiod_line_event_wait(line, &ts);
        if (ret < 0)
        {
            piboxLogger(LOG_ERROR, "GPIO wait event notification failed.\n");
            unsetCLIFlag(CLI_GPIO);
            continue;
        }
        else if (ret == 0)
        {
            piboxLogger(LOG_TRACE4, "Wait event notification on line #%u timeout\n", line_num);
            continue;
        }

        ret = gpiod_line_event_read(line, &event);
        if (ret < 0)
        {
            piboxLogger(LOG_ERROR, "GPIO read of last event notification failed\n");
            unsetCLIFlag(CLI_GPIO);
            continue;
        }
        piboxLogger(LOG_INFO, "GPIO pushbutton event!\n");

        if( isCLIFlagSet(CLI_AP_STARTING) )
        {
            piboxLogger(LOG_INFO, "AP startup is in progress - ignoring keypress.\n");
            continue;
        }

        if ( isCLIFlagSet(CLI_DEBOUNCE) )
        {
            piboxLogger(LOG_WARN, "Button press while debounce enabled - ignoring.\n");
            continue;
        }

        /* A valid event showed up.  Start the debounce timer. */
        setCLIFlag(CLI_DEBOUNCE);
        (void)g_timeout_add(1250, (GSourceFunc)debounceReset, NULL);

        /* Either start or stop AP network. */
        gpio_state = (gpio_state == PINET_GPIO_NORMAL)? PINET_GPIO_AP : PINET_GPIO_NORMAL;
        switch(gpio_state)
        {
            /* Going to AP mode. */
            case PINET_GPIO_AP:
                piboxLogger(LOG_INFO, "Going to AP mode.\n");
                kill(0, SIGUSR1);
                break;

            /* Going to normal mode. */
            case PINET_GPIO_NORMAL:
                piboxLogger(LOG_INFO, "Going to normal mode.\n");
                kill(0, SIGUSR2);
        }
        unsetCLIFlag(CLI_DEBOUNCE);
    }

    /* Shutdown libgpiod. */
    gpiod_line_release(line);

    piboxLogger(LOG_INFO, "gpio thread is exiting.\n");
    gpioIsRunning = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Public functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   startGPIOMonitor
 * Prototype:  void startGPIOMonitor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Returns:
 * False on error.  True on success.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownGPIOMonitor().
 *========================================================================*/
gboolean
startGPIOMonitor( void )
{
    int rc;
    int ret;
    char *chipname = "gpiochip0";
    unsigned int line_num = PINET_GPIO_LED_PIN;

    chip = gpiod_chip_open_by_name(chipname);
    if (!chip)
    {
        piboxLogger(LOG_ERROR, "Open chip failed for %s\n", chipname);
        return(FALSE);
    }

    led_line = gpiod_chip_get_line(chip, line_num);
    if (!led_line)
    {
        piboxLogger(LOG_ERROR, "Get line failed for GPIO LED pin %d\n", line_num);
        return(FALSE);
    }

	ret = gpiod_line_request_output(led_line, CONSUMER_LED, 0);
	if (ret != 0)
    {
		piboxLogger(LOG_ERROR, "LED GPIO line as output failed: %s\n", strerror(errno));
        return(FALSE);
	}

    setCLIFlag( CLI_GPIO );

    /* Create a thread to manage GPIO events. */
    rc = pthread_create(&gpioThread, NULL, &gpio, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create processor thread: %s\n", strerror(rc));
        gpiod_chip_close(chip);
        exit(FALSE);
    }
    piboxLogger(LOG_INFO, "Started GPIO thread.\n");
    return(TRUE);
}

/*========================================================================
 * Name:   shutdownGPIOMonitor
 * Prototype:  void shutdownGPIOMonitor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownGPIOMonitor( void )
{
    int timeOut = 1;

    unsetCLIFlag( CLI_GPIO );
    while ( isProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "Timed out waiting on processor thread to shut down.\n");
            break;
        }
    }
    pthread_detach(gpioThread);

    if (led_line)
        gpiod_line_release(led_line);
    if (chip)
        gpiod_chip_close(chip);
    piboxLogger(LOG_INFO, "gpio thread has shut down.\n");
}

/*========================================================================
 * Name:   enableLEDs
 * Prototype:  void enableLEDs( int state )
 *
 * Description:
 * Enable LED flashing.  Sets flash mode to 'state'.
 *
 * Input Arguments:
 * int state        One of: PINET_LED_OFF, PINET_LED_AP_ON, PINET_LED_NETWORK_RESTARTING, PINET_LED_NORMAL
 *
 * Notes:
 * LEDs will remain in the specified pattern until the state is set to
 * PINET_LED_OFF using either disableLEDs() or having set PINET_LED_NORMAL
 * and letting that run it's course.
 *========================================================================*/
void 
enableLEDs( int state )
{
    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &gpioMutex );

    // Do nothing if timer is already enabled.
    if ( led_timer )
    {
        piboxLogger(LOG_INFO, "Timer already enabled.\n");
        pthread_mutex_unlock( &gpioMutex );
        return;
    }

    piboxLogger(LOG_INFO, "Setting LED timer for state: %d.\n", state);
    led_state = state;
    led_timer = g_timeout_add(1000, (GSourceFunc)_flashLEDs, NULL);
    pthread_mutex_unlock( &gpioMutex );
}

/*========================================================================
 * Name:   disableLEDs
 * Prototype:  void disableLEDs( void )
 *
 * Description:
 * Change the flash pattern to the specified state.
 *
 * Input Arguments:
 * int state        One of: PINET_LED_OFF, PINET_LED_AP_ON, PINET_LED_NETWORK_RESTARTING, PINET_LED_NORMAL
 *
 * Notes:
 *========================================================================*/
void 
disableLEDs( void )
{
    piboxLogger(LOG_INFO, "Disabling LEDs.\n");
    pthread_mutex_lock( &gpioStateMutex );
    led_state = PINET_LED_OFF;
    pthread_mutex_unlock( &gpioStateMutex );
}

/*========================================================================
 * Name:   setLEDState
 * Prototype:  void setLEDState( int state )
 *
 * Description:
 * Set the LED flash state.
 *
 * Input Arguments:
 * int state        One of: PINET_LED_OFF, PINET_LED_AP_ON, PINET_LED_NETWORK_RESTARTING, PINET_LED_NORMAL
 *========================================================================*/
void 
setLEDState( int state )
{
    piboxLogger(LOG_INFO, "Setting LED state to %d\n", state);
    pthread_mutex_lock( &gpioStateMutex );
    led_state = state;
    pthread_mutex_unlock( &gpioStateMutex );
}
#endif
