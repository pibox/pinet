/*******************************************************************************
 * pinet
 *
 * pinet.c:  program main
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PINET_C

#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

/* Local prototypes */
gboolean imageTouchGTK( gpointer arg );
void *do_drawing();
static void updatePSK( char* pw );
void pinetExit( void );

GtkWidget           *darea;
GdkPixmap           *pixmap = NULL;
cairo_t             *cr = NULL;
guint               homeKey = -1;
GtkWidget           *window;
int                 currently_drawing = 0;
char                *passwd;
char                textURL[1024];

/* Default image dir */
char *defaultImagePath = NULL;

/* 
 * Font sizes depend on screen size.
 * Defaults to a normal sized screen.
 * Small screens reduce this size.
 */
gint largeFont  = 25;
gint mediumFont = 13;
gint smallFont  = 10;
gint summaryFont= 20;
gint listFont   = 20;

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * ========================================================================
 */
static void
sigHandler( int sig )
{
    switch (sig)
    {
        case SIGHUP:
            pinetExit();
            break;

        case SIGTERM:
            pinetExit();
            break;

        case SIGINT:
            pinetExit();
            break;

        case SIGQUIT:
            pinetExit();
            break;
    }
}

/*
 *========================================================================
 * Name:   deg2rad
 * Prototype:  double deg2rad( double )
 *
 * Description:
 * Converts degree angles to radians.
 *========================================================================
 */
static double
deg2rad( double degrees )
{
    return((double)((double)degrees * ( (double)M_PI/(double)180.0 )));
}

/*
 *========================================================================
 * Name:   updateText
 * Prototype:  double updateText( int )
 *
 * Description:
 * Set the text to display on the UI.
 *========================================================================
 */
static void
updateText( int shutdown )
{
    char    *text;

    if ( shutdown == 1 )
    {
        /* Note that network is restarting. */
        piboxLogger(LOG_INFO, "Setting textURL for shutdown.\n");
        sprintf(textURL, "Network is restarting.  Please be patient.");
    }
    else
    {
        piboxLogger(LOG_INFO, "Setting textURL for runtime.\n");
        if ( isCLIFlagSet(CLI_TOUCH) )
            text = "Touch the screen to exit when done.";
        else
            text = "Press ESC to exit when done.";

        piboxLogger(LOG_INFO, "URL: http://%s:%d\n", DEFAULT_AP_IP, DEFAULT_PORT);
        piboxLogger(LOG_INFO, "SSID: %s, PW: %s\n", DEFAULT_SSID, passwd);
        piboxLogger(LOG_INFO, "MAC: %s\n", cliOptions.macaddr);
        if ( cliOptions.ethIP )
        {
            piboxLogger(LOG_INFO, "eth IP: %s\n", cliOptions.ethIP);
            sprintf(textURL,
                "SSID: %s | Password: %s\neth IP: %s WiFi MAC: %s\nGo to http://%s:%d to configure network.\n%s",
                DEFAULT_SSID, passwd, cliOptions.ethIP, cliOptions.macaddr, DEFAULT_AP_IP, DEFAULT_PORT, text);
        }
        else
        {
            piboxLogger(LOG_INFO, "eth IP: none\n");
            sprintf(textURL,
                "SSID: %s | Password: %s\neth IP: none WiFi MAC: %s\nGo to http://%s:%d to configure network.\n%s",
                DEFAULT_SSID, passwd, cliOptions.macaddr, DEFAULT_AP_IP, DEFAULT_PORT, text);
        }
    }
}

/*
 * ========================================================================
 * Name:   timer_exec
 * Prototype:  gboolean timer_exec( GtkWidget *window )
 *
 * Description:
 * Animation engine - causes updates for the animation on a timed interval.
 *
 * Notes:
 * Taken basically verbatim from the Cairo web site tutorial on animation.
 * ========================================================================
 */
static gboolean
quit(GtkWidget * window)
{
    static int  count=0;

    struct stat stat_buf;
    if ( stat(PINET_DONE, &stat_buf) == 0 )
    {
        unlink(PINET_DONE);
        // if ( !isCLIFlagSet(CLI_RESTART) )
        gtk_main_quit();
        return(FALSE);
    }
    else
    {
        // Wait only 10 seconds, max.
        count++;
        if ( count == 40 )
        {
            gtk_main_quit();
            return(FALSE);
        }
        return(TRUE);
    }
}

/*
 *========================================================================
 * Name:   pinetExit
 * Prototype:  void pinetExit( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pinetExit()
{
    // Reset the hostapd-psk file.
    piboxLogger(LOG_INFO, "Resetting psk file.\n");
    updatePSK(DEFAULT_PSK);

    piboxLogger(LOG_INFO, "Calling updateText()\n");
    updateText( 1 );
    piboxLogger(LOG_INFO, "Restarting system network.\n");

    /* Resart system network. */
    setCLIFlag(CLI_RESTART);
    restartNetwork();

    /* Wait a bit for network restart, but allow the animation to continue to spin till we quit. */
    (void)g_timeout_add(1000, (GSourceFunc)quit, NULL);
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( REGION_T *pt )
 *
 * Description:
 * Handler for absolute touch reports.  For PiNet, any touch just means
 * to exit.
 *========================================================================
 */
void
imageTouch( REGION_T *pt )
{
    g_idle_add( (GSourceFunc)imageTouchGTK, (gpointer)pt );
}

gboolean
imageTouchGTK( gpointer arg )
{
    pinetExit();
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be
 * handled.
 *
 * Notes:
 * Format is
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayTouch() == PIBOX_TOUCH )
    {
        setCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Touch screen support enabled.\n");
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Not a touch screen.\n");
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                // gtk_main_quit();
                pinetExit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            // gtk_main_quit();
            pinetExit();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                // gtk_main_quit();
                pinetExit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   drawText
 * Prototype:  void *drawText( cairo_t, int, int, char *, int )
 *
 * Description:
 * Updates the help text on the display.
 *========================================================================
 */
void
drawText( cairo_t *cr, int pixmap_width, int pixmap_height, char *textPtr, int multiplier)
{
    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    gint                    fontSize = summaryFont;

    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        fontSize = mediumFont;

    layout = pango_cairo_create_layout(cr);
    sprintf(buf, "Monospace Bold %d", fontSize);
    desc = pango_font_description_from_string(buf);
    pango_font_description_set_absolute_size(desc, fontSize*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
    pango_layout_set_width(layout, ((int)(pixmap_width)-20)*PANGO_SCALE);
    pango_layout_set_spacing(layout, 1);
    pango_layout_set_single_paragraph_mode(layout, FALSE);
    pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);

    /* Initialize attributes. */
    // cairo_translate(cr, 10, 0);
    attrs = pango_attr_list_new();
    pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
    pango_layout_set_attributes (layout, attrs);

    piboxLogger(LOG_TRACE4, "text: %s\n", textPtr);
    pango_layout_set_text(layout, textPtr, -1);
    pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
    piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
    cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
    pango_cairo_update_layout(cr, layout);
    /* Don't need to center horizontally because pango_layout_set_alignment does that for us. */
    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        cairo_translate(cr, 0, pixmap_height-(pangoRectangle.height*multiplier)-5);
    else
        cairo_translate(cr, 0, pixmap_height-(pangoRectangle.height*multiplier)+25);
    pango_cairo_show_layout(cr, layout);

    /* Cleanup pango */
    pango_attr_list_unref(attrs);
    g_object_unref(layout);
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void *do_drawing( void *ptr )
 *
 * Description:
 * Updates the animation area of the display.
 *========================================================================
 */
void *
do_drawing(void *ptr)
{
    static int              imageID = 0;        // Determines which frame of animation we're generating.
    static int              direction = 1;      // Sets increment/decrement of imageId cycling.
    static int              pause = 0;          // Allows pausing momentarily at the top of the rotation.
    static GdkPixbuf        *image = NULL;
    static GdkPixbuf        *imageScaled = NULL;
    static int              image_width=0, image_height=0;
    int                     pixmap_width, pixmap_height;
    cairo_t                 *cr_pixmap;
    cairo_t                 *cr;
    cairo_t                 *image_cr;
    cairo_surface_t         *cst;
    cairo_surface_t         *surface;
    cairo_pattern_t         *pattern;
    cairo_matrix_t          matrix;
    cairo_format_t          format;

    /* Prevent simultaneous calls to this function. */
    currently_drawing = 1;

    // Load the image to display on the first pass through.
    if ( image == NULL )
    {
        image = gdk_pixbuf_new_from_file(defaultImagePath, NULL);
        if ( image == NULL )
        {   
            piboxLogger(LOG_ERROR, "Failure loading image: %s\n", defaultImagePath);
            currently_drawing = 0;
            return NULL;
        }
        if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        {
            image_width = gdk_pixbuf_get_width(image);
            image_height = gdk_pixbuf_get_width(image);
            imageScaled = gdk_pixbuf_scale_simple(image, image_width*.5, image_height*.5, GDK_INTERP_BILINEAR);
            g_object_unref(image);
            image = imageScaled;
        }
        image_width = gdk_pixbuf_get_width(image);
        image_height = gdk_pixbuf_get_width(image);
        piboxLogger(LOG_INFO, "Loaded image (%d x %d): %s\n", image_width, image_height, defaultImagePath);
    }

    /* Need to be in main thread to get drawable information. */
    gdk_threads_enter();
    gdk_drawable_get_size(pixmap, &pixmap_width, &pixmap_height);
    gdk_threads_leave();

    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, pixmap_width, pixmap_height);
    cr = cairo_create(cst);

    /* Blank (re: black it out) the surface */
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_rectangle(cr, 0, 0, pixmap_width, pixmap_height);
    cairo_fill(cr);

    /* Create a secondary surface to draw, position and rotate the image. */
    format = (gdk_pixbuf_get_has_alpha (image)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
    surface = cairo_image_surface_create (format, image_width, image_height);
    image_cr = cairo_create (surface);
    pattern = cairo_pattern_create_for_surface (surface);

    /* Draw the image on the Cairo surface. */
    gdk_cairo_set_source_pixbuf(image_cr, image, 0, 0);
    cairo_paint(image_cr);

    /* Translate the rotation point of the image. */
    cairo_matrix_init_identity (&matrix);
    // cairo_matrix_translate (&matrix, (double)(LOGO_CENTER_X), (double)(LOGO_CENTER_Y));
    cairo_matrix_translate (&matrix, (double)(image_width/2), (double)(image_height/2));

    /* Scale the image. */
    if ( pause == 0 )
        cairo_matrix_scale (&matrix, 1+12*((double)imageID*0.016), 1+12*((double)imageID*0.016));

    /* Rotate the image: 15 over 2 seconds = 12 degrees per imageID */
    cairo_matrix_rotate (&matrix, deg2rad(imageID*6));

    /* Move to the center of the drawing area window */
    cairo_matrix_translate (&matrix, -pixmap_width/2, -pixmap_height/2);
    cairo_pattern_set_matrix (pattern, &matrix);

    cairo_set_source (cr, pattern);
    cairo_paint(cr);

    cairo_pattern_destroy (pattern);
    cairo_destroy(image_cr);
    cairo_surface_destroy(surface);

    /* Draw informative text below the animation. */
    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        drawText(cr, pixmap_width, pixmap_height, textURL, 1);
    else
        drawText(cr, pixmap_width, pixmap_height, textURL, 2);

    /* Put it all on the visible surface. */
    gdk_threads_enter();
    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    gdk_threads_leave();

    cairo_destroy(cr);
    cairo_surface_destroy(cst);

    /* Manage the delay and direction of the animation. */
    if ( pause == 0 )
    {
        if ( direction )
        {
            imageID++;
            if ( imageID == 30 )
            {
                /* This allows image to be fully upright. */
                imageID = 0;
                direction=0;
                pause++;
            }
        }
        else
        {
            imageID--;
            if ( imageID == 0 )
            {
                direction=1;
                pause++;
            }
        }
    }
    else
    {
        pause++;
        if ( pause == 30 )
        {
            pause = 0;

            /* If we should be headed back, then reset the imageID */
            if ( direction == 0 )
                imageID = 29;
        }
    }

    currently_drawing = 0;

    return NULL;
}

/*
 *========================================================================
 * Name:   on_window_configure_event
 * Prototype:  void on_window_configure_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Resize the pixmap we draw in if the window size changes (which is shouldn't ever do).
 *========================================================================
 */
gboolean
on_window_configure_event( GtkWidget * da, GdkEventConfigure * event, gpointer user_data )
{
    GdkPixmap   *tmppixmap;
    int         minw, minh;
    static int  oldw = 0;
    static int  oldh = 0;

    /* Resize pixmap if our window has been resized */
    if (oldw != event->width || oldh != event->height)
    {
        tmppixmap = gdk_pixmap_new(GTK_WIDGET(user_data)->window, event->width,  event->height, -1);

        /*
         * Copy the contents of the old pixmap to the new pixmap.  This keeps ugly uninitialized
         * pixmaps from being painted upon resize.
         */
        minw = oldw;
        minh = oldh;
        if( event->width < minw )
            minw =  event->width;
        if( event->height < minh )
            minh =  event->height;
        gdk_draw_drawable(tmppixmap, 
                GTK_WIDGET(user_data)->style->fg_gc[GTK_WIDGET_STATE(GTK_WIDGET(user_data))], 
                pixmap, 0, 0, 0, 0, minw, minh);

        /* Replace old pixmap with resized one. */
        g_object_unref(pixmap);
        pixmap = tmppixmap;
    }

    oldw = event->width;
    oldh = event->height;

    return TRUE;
}

/*
 *========================================================================
 * Name:   on_window_expose_event
 * Prototype:  void on_window_expose_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Update an exposed area with the associated region from the pixmap.
 *========================================================================
 */
gboolean
on_window_expose_event( GtkWidget * da, GdkEventExpose * event, gpointer user_data )
{
    /* Copy the area that was exposed. */
    gdk_draw_drawable(GTK_WIDGET(user_data)->window,
        da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap,
        event->area.x, event->area.y,
        event->area.x, event->area.y,
        event->area.width, event->area.height);
    return TRUE;
}

/*
 *========================================================================
 * Name:   updatePSK
 * Prototype:  void updatePSK( char * )
 *
 * Description:
 * Update the hostapd psk file with the specified pw.
 *========================================================================
 */
static void
updatePSK( char* pw )
{
    FILE    *fd;
    char    *filename;

    if ( isCLIFlagSet(CLI_TEST) )
        filename = F_PSK_T;
    else
        filename = F_PSK;

    piboxLogger(LOG_INFO, "Writing psk file: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Error opening psk file: %s\n", strerror(errno));
        return;
    }
    fprintf(fd, "00:00:00:00:00:00 %s\n", pw);
    fclose(fd);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget   *vbox;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // One row: A spinning animation.
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /*
     * The spinning animation area
     */
    darea = gtk_drawing_area_new();
    gtk_widget_set_size_request( darea, piboxGetDisplayWidth(), piboxGetDisplayHeight());
    gtk_box_pack_start (GTK_BOX (vbox), darea, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_window_expose_event), darea);
    g_signal_connect(G_OBJECT(darea), "configure_event", G_CALLBACK(on_window_configure_event), darea);

    /* Make the main window die when destroy is issued. */
    // g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK (gtk_main_quit), NULL);
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK (pinetExit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 2048, 1024);
    gtk_window_set_title(GTK_WINDOW(window), "pinet");

    return window;
}

/*
 * ========================================================================
 * Name:   timer_exec
 * Prototype:  gboolean timer_exec( GtkWidget *window )
 *
 * Description:
 * Animation engine - causes updates for the animation on a timed interval.
 *
 * Notes:
 * Taken basically verbatim from the Cairo web site tutorial on animation.
 * ========================================================================
 */
static gboolean 
timer_exec(GtkWidget * window)
{
    static gboolean     first_execution = TRUE;
    static pthread_t    thread_info;
    int                 drawing_status;
    int                 width, height;

    /* 
     * Safe function to get the value of currently_drawing to
     * avoid multithreading issues.
     */
    drawing_status = g_atomic_int_get(&currently_drawing);

    /*
     * If not currently drawing anything, launch a thread to
     * update the pixmap.
     */
    if( drawing_status == 0 )
    {
        if ( first_execution != TRUE )
        {
            pthread_join(thread_info, NULL);
        }
        pthread_create( &thread_info, NULL, do_drawing, NULL);
    }

    /* Update the animation. */
    gdk_drawable_get_size(pixmap, &width, &height);
    gtk_widget_queue_draw_area(darea, 0, 0, width, height);

    first_execution = FALSE;

    return TRUE;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    cwd[512];
    char    gtkrc[1024];
    char    filePath[PATH_MAX];
    char    *dirPath = F_SCRIPT_DIR;  // Test mode is not supported because it can muck up the network.

    GtkWidget *window;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Handle signals */
    signal(SIGQUIT, sigHandler);
    signal(SIGTERM, sigHandler);
    signal(SIGHUP, sigHandler);
    signal(SIGINT, sigHandler);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    
    /* Get the current working directory. */
    memset(cwd, 0, 512);
    getcwd(cwd, 512);

    /* Set the image_dir */
    if ( isCLIFlagSet( CLI_TEST) )
    {   
        defaultImagePath =
                g_malloc0(strlen(cwd) + strlen(F_IMAGE_DIR_T) + strlen(F_DEFAULT_IMAGE) + 3);
        sprintf(defaultImagePath, "%s/%s/%s", cwd, F_IMAGE_DIR_T, F_DEFAULT_IMAGE);
    }
    else
    {   
        defaultImagePath = g_malloc0(strlen(F_IMAGE_DIR) + strlen(F_DEFAULT_IMAGE) + 2);
        sprintf(defaultImagePath, "%s/%s", F_IMAGE_DIR, F_DEFAULT_IMAGE);
    }

    /* Read environment config for keyboard behaviour */
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();

    /*
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }

    /*
     * We need to initialize all these functions so that gtk knows
     * to be thread-aware.
     */
    gdk_threads_init();
    gdk_threads_enter();

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse(gtkrc);
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    /* 
     * Initialize pixmap for animation.  
     * Because we will be painting our pixmap manually during expose events 
     * we can turn off gtk's automatic painting and double buffering routines.
     */
    pixmap = gdk_pixmap_new(darea->window,500,500,-1);
    gtk_widget_set_app_paintable(darea, TRUE);
    gtk_widget_set_double_buffered(darea, FALSE);

    /* Generate a random password for the AP */
    passwd = genRandomPassword();

    /* Update the hostapd-psk file. */
    piboxLogger(LOG_INFO, "Updating psk file.\n");
    updatePSK(passwd);

    /* Where are our startup scripts? */
    if ( isCLIFlagSet( CLI_TEST) )
        dirPath =  F_SCRIPT_DIR_T;
    else
        dirPath =  F_SCRIPT_DIR;

    /* Start the AP: The "imwww" package install created stamp files for us.  */
    piboxLogger(LOG_INFO, "Starting AP.\n");
    if ( !isCLIFlagSet( CLI_TEST) )
    {
        sprintf(filePath, "%s/%s", dirPath, AP_START);
        system(filePath);
    }

    /* Start the web server */
    if ( !isCLIFlagSet( CLI_TEST) )
    {
        sprintf(filePath, "%s/%s", dirPath, WEBSERVER_START);
        piboxLogger(LOG_INFO, "Starting imwww: %s\n", filePath);
        system(filePath);
    }

    /* Get our IP address so we can print a message on the animation. */
    updateText( 0 );

    /* A timer will update the animation. */
    (void)g_timeout_add(66, (GSourceFunc)timer_exec, NULL);

    /* Start threads */
    startNetworkProcessor();

    gtk_main();
    gdk_threads_leave();

    /* Stop threads */
    shutdownNetworkProcessor();

    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}

