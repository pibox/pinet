/*******************************************************************************
 * PiBox application library
 *
 * handleAP.c:  Manage start and stop of the AP.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef HANDLEAP_H
#define HANDLEAP_H

#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef HANDLEAP_C

#endif /* !HANDLEAP_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef HANDLEAP_C

extern gboolean startAPProcessor( void );
extern void     shutdownAPProcessor( void );
extern void     handleAP( void );

#endif /* !HANDLEAP_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !HANDLEAP_H */
