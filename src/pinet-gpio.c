/*******************************************************************************
 * pinet
 *
 * pinet.c:  program main
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PINET_C

#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pinet.h"

GMainLoop *loop;

/* Default image dir */
char *defaultImagePath = NULL;
char *passwd;

/*
 * ========================================================================
 * Name:   quit
 * Prototype:  gboolean quit( void *state )
 *
 * Description:
 * Function for resetting the app after network restart.
 *
 * Notes:
 * This function returns the LED state to NORMAL once the network restart
 * is complete but we don't really quit in the GPIO version.
 * ========================================================================
 */
static gboolean
quit(void *state)
{
    static int  count=0;
    struct stat stat_buf;

    if ( state )
    {
        /* We're waiting for the network to restart. */
        if ( stat(PINET_DONE, &stat_buf) == 0 )
        {
			/* Restart complete - show it to user and stop timer. */
            unsetCLIFlag(CLI_AP_RUNNING);
            unlink(PINET_DONE);
            setLEDState( PINET_LED_NORMAL );
            sleep(2);
			count=0;
            return(FALSE);
        }
        else
        {
            // Wait 120 seconds, max.
            if ( count++ == 60 )
            {
				/* Timeout waiting on restart - show it to user and stop timer. */
                unsetCLIFlag(CLI_AP_RUNNING);
                unlink(PINET_DONE);
                setLEDState( PINET_LED_NORMAL );
                sleep(2);
				count=0;
                return(FALSE);
            }
            return(TRUE);
        }
    }
    else
    {
        /* We're really quitting. */
        unlink(PINET_DONE);
        setLEDState( PINET_LED_NORMAL );
        sleep(2);
        g_main_loop_quit(loop);
        return(FALSE);
    }
}

/*
 *========================================================================
 * Name:   pinetExit
 * Prototype:  void pinetExit( void )
 *
 * Description:
 * Restart the network (but doesn't really exit in GPIO version).
 *========================================================================
 */
void
pinetExit()
{
    /* If a request comes in while we're starting or stopping then just ignore it. */
    if ( isCLIFlagSet( CLI_RESTART ) )
        return;
    setCLIFlag(CLI_RESTART);

    /* Resart system network. */
    setLEDState( PINET_LED_NETWORK_RESTARTING );
    restartNetwork();

    /* Wait a bit for network restart. */
    (void)g_timeout_add(1000, (GSourceFunc)quit, (gpointer)1);
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            // TBD
            break;

        case SIGTERM:
            piboxLogger(LOG_INFO, "SIGTERM\n");
            (void)g_timeout_add(1000, (GSourceFunc)quit, NULL);
            break;

        case SIGQUIT:
            piboxLogger(LOG_INFO, "SIGQUIT\n");
            (void)g_timeout_add(1000, (GSourceFunc)quit, NULL);
            break;

        case SIGINT:
            piboxLogger(LOG_INFO, "SIGINT\n");
            (void)g_timeout_add(1000, (GSourceFunc)quit, NULL);
            break;

        case SIGUSR1:
            /* We are being asked to start the AP. */
            piboxLogger(LOG_INFO, "SIGUSR1: Starting AP.\n");
            handleAP();
            break;

        case SIGUSR2:
            /* Stop AP and restart network, possibly with new network config. */
            piboxLogger(LOG_INFO, "SIGUSR2: Stopping AP and restarting network.\n");
            pinetExit();
            break;
    }
}

/*
 * ========================================================================
 * Handle daemonizing of the application.
 * See http://www.enderunix.org/docs/eng/daemon.php
 * ========================================================================
 */
static void
daemonize( void )
{
    int                 pid, fd;
    char                pidStr[7];
    struct sigaction    sa;
    FILE                *pd;

    if ( isCLIFlagSet(CLI_DAEMON) )
    {
        fprintf(stderr, "%s: Enabling  daemonize mode.\n", PROG);

        pid = fork();
    
        /* On error, print a message and exit */
        if ( pid<0 )
        {
            fprintf(stderr, "%s: Failed to daemonize: %s\n", PROG, strerror(errno));
            exit(1);
        }
    
        /* If this is the parent, we exit immediately. */
        if ( pid>0 )
        {
            /* Save the child's PID. */
            fprintf(stderr, "%s: Saving PID to %s\n", PROG, PIDFILE);
            pd = fopen(PIDFILE, "w");
            fprintf(pd, "%d", pid);
            fclose(pd);
    
            fprintf(stderr, "%s: parent process is exiting.\n", PROG);
            exit(0);
        }
    
        /* The child needs some work to make it a daemon. */
        fprintf(stderr, "%s: Setting up daemon\n", PROG);
    
        /* Get our own process group and detach the controlling terminal. */
        setsid();
    
        /* File creation mask - for safety sake */
        umask(027);
    
        /* Change to a meaningful working directory. */
        mkdir(RUN_DIR, 0700);
        if ( chdir(RUN_DIR) == -1 )
            fprintf(stderr, "Failed to change to %s\n", RUN_DIR);
    
        /* Set up a lock file so only one instance can run. */
        cliOptions.lockfd = open(LOCK_FILE,O_RDWR|O_CREAT,0640);
        if ( cliOptions.lockfd<0 )
        {
            fprintf(stderr, "%s: Failed to get lock file.  Exiting.\n", PROG);
            exit(1);
        }
    
        if (lockf(cliOptions.lockfd,F_TLOCK,0)<0)
        {
            /* only first instance continues */
            fprintf(stderr, "%s: is already running.\n", PROG);
            exit(0);
        }
    
        /* Save child pid to lockfile */
        sprintf(pidStr,"%d\n",getpid());
        if ( write(cliOptions.lockfd, pidStr, strlen(pidStr)) == -1 )
            fprintf(stderr, "Failed to save child pid to lockfile.\n");
    }

    /* Setup for basic signal handling */
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGQUIT, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGQUIT.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGINT.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR1.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR2, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR2.\n", PROG);
        exit(1);
    }

    if ( isCLIFlagSet(CLI_DAEMON) )
    {
        /* Close inherited descriptors, then get our own. */
        for (fd=getdtablesize(); fd>=0; --fd)
            close(fd);
        fd = open("/dev/null", O_RDWR);    stdin  = fdopen(fd, "r"); /* stdin  */
        fd = open("/dev/console", O_RDWR); stdout = fdopen(fd, "w"); /* stdout */
        fd = dup(fd);                      stderr = fdopen(fd, "w"); /* stderr */
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    // Daemonize (if requested) and setup signal handling
    daemonize();

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerSetFlags(LOG_F_TIMESTAMP);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    else
        piboxLogger(LOG_INFO, "No log file configured.\n");
    
    /* Start threads */
    startNetworkProcessor();
    if ( startGPIOMonitor() == FALSE )
    {
        piboxLogger(LOG_ERROR, "GPIO monitoring failed to initialize.\n");
        goto failed_monitor;
    }
    if ( startAPProcessor() == FALSE )
    {
        piboxLogger(LOG_ERROR, "AP processor failed to initialize.\n");
        goto failed_ap_processor;
    }

    /* Loop till forced into an exit with signals. */
    loop = g_main_loop_new ( NULL , FALSE );
    g_main_loop_run (loop);
    g_main_loop_unref(loop);

    /* Stop threads */
    shutdownAPProcessor();

failed_ap_processor:
    shutdownGPIOMonitor();

failed_monitor:
    shutdownNetworkProcessor();

    piboxLoggerShutdown();
    return 0;
}
